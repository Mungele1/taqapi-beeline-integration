package com.buzz.taq.email.beeline.utility;

import java.math.BigInteger;

/**
 * The Class beeline constants.
 *
 */
public class BeelineConstants {

	/** The Constant APP_SERVER_URL. */
	public static final String APP_SERVER_URL = "APP_SERVER_URL";

	public static final BigInteger VENDOR_ID = new BigInteger("172722679948397703697763120281494243487");

	public static final String GENPACT = "Genpact";

	public static final String SYSTEM = "System";

	public static final String ADDED_BEELINE_JOBS = "Added beeline jobs";

	public static final String STATUS_CANCELLED = "Cancelled";

	public static final String STATUS_FILLED = "Filled";

	public static final String STATUS_ONHOLD = "OnHold";

	public static final String STATUS_OPEN = "Open";

}
