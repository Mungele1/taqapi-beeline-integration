package com.buzz.taq.email.beeline.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.buzz.taq.email.beeline.entity.JobsHistory;

/**
 * The Interface JobsHistoryRepo.
 *
 */
@Repository
public interface JobsHistoryRepo extends CrudRepository<JobsHistory, BigInteger> {

}
