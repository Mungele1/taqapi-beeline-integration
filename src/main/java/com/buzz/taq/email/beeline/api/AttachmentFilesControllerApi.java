package com.buzz.taq.email.beeline.api;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.buzz.taq.email.beeline.models.StandardResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-10-03T06:16:30.521-05:00")

@Api(value = "AttachmentFilesController", description = "the AttachmentFilesController API")
public interface AttachmentFilesControllerApi {

	@ApiOperation(value = "Upload attachment files.", nickname = "postAttachmentFile", notes = "", response = StandardResponse.class, tags = {
			"attachment-files-controller", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "HTTP OK", response = StandardResponse.class),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "Not Found") })
	@RequestMapping(value = "/api/attachment-files", consumes = { "multipart/form-data" }, method = RequestMethod.POST)
	ResponseEntity<StandardResponse> postAttachmentFile(
			@ApiParam(value = "file detail") @Valid @RequestPart("uploadfile") final MultipartFile uploadfile);
}
