package com.buzz.taq.email.beeline.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "contacts")
@Setter
@Getter
@ToString
public class Contacts implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true)
	private BigInteger id;

	@Column(name = "ENTITYTYPE", nullable = false)
	private String entityType;

	@Column(name = "ENTITYID", nullable = false)
	private BigInteger entityId;

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "PHONE", nullable = false)
	private String phone;

	@Column(name = "EMAIL", nullable = false)
	private String email;

	@Column(name = "PROFILE")
	private String profile;

	@Column(name = "CREATED", nullable = false)
	private Timestamp created;

	@Column(name = "CREATED_BY", nullable = false)
	private String createdBy;

	@Column(name = "UPDATED")
	private Timestamp updated;

	@Column(name = "UPDATED_BY")
	private String updatedBy;

	@Column(name = "ROLE")
	private String role;

}
