package com.buzz.taq.email.beeline.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "jobs")
@Setter
@Getter
@ToString
public class AttachmentFiles implements Serializable {
	// private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	// @Column(name = "ID", unique = true, nullable = false)
	private BigInteger id;

	@Column(name = "VENDORID", nullable = false)
	private BigInteger vendorId;

	@Column(name = "VENDORCONTACTID")
	private BigInteger vendorContactID;

	@Column(name = "TITLE", nullable = false)
	private String title;

	@Column(name = "COUNTRY")
	private String country;

	@Column(name = "JOBDESC", nullable = false)
	private String jobDesc;

	@Column(name = "STATECD")
	private String stateCD;

	@Column(name = "CITY", nullable = false)
	private String city;

	@Column(name = "CLIENT")
	private String client;

	@Column(name = "RATE", nullable = false)
	private Double rate;

	@Column(name = "EXPENSE")
	private String expense;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "JBSOURCE")
	private String jbSource;

	@Column(name = "SUBMISSIONLIMIT")
	private Long submissionLimit;

	@Column(name = "POSITIONS")
	private Long positions;

	@Column(name = "VISARESTRICTION")
	private String visaRestriction;

	@Column(name = "DURATION")
	private String duration;

	@Column(name = "CREATED", nullable = false)
	private Timestamp created;

	@Column(name = "CREATED_BY", nullable = false)
	private String createdBy;

	@Column(name = "UPDATED")
	private Timestamp updated;

	@Column(name = "UPDATED_BY")
	private String updatedBy;

	@Column(name = "ACTIVE")
	private Character active;

	@Column(name = "REQUIREMENTS")
	private String requirements;

	@Column(name = "INTERNALCOMMENTS")
	private String internalComments;

	@Column(name = "ZIP")
	private String zip;

	@Column(name = "VMSID")
	private String vmsId;

	@Column(name = "TAG")
	private String tag;

	@Column(name = "PUBLISH")
	private Character publish;

	@Column(name = "AUTOEXPIRE") // Date in job sheet
	private Timestamp autoExpire;

	@Column(name = "JOBTYPE")
	private Integer jobType;

	// @Column(name = "CATEGORY")
	// private Long category;

}
