package com.buzz.taq.email.beeline.models;

import java.sql.Timestamp;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class BeelineSummary {

	private Timestamp date;
	private String task;
	private int createdjobs;
	private List<String> edittedJobs;
	private List<String> deactivatedJobs;
	private List<String> error;

}
