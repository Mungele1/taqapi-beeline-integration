package com.buzz.taq.email.beeline.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.buzz.taq.email.beeline.entity.AttachmentFiles;

/**
 * The Interface AttachmentFilesRepo.
 *
 */
@Repository
public interface AttachmentFilesRepo extends CrudRepository<AttachmentFiles, BigInteger> {

	List<AttachmentFiles> findByVmsId(String vmsId);

}
