package com.buzz.taq.email.beeline.service;

import java.io.InputStreamReader;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.buzz.taq.config.integrationconfig.entity.IntegrationConfig;
import com.buzz.taq.config.integrationconfig.repository.IntegrationConfigRepo;
import com.buzz.taq.email.beeline.entity.AttachmentFiles;
import com.buzz.taq.email.beeline.entity.Contacts;
import com.buzz.taq.email.beeline.entity.JobsHistory;
import com.buzz.taq.email.beeline.repository.AttachmentFilesRepo;
import com.buzz.taq.email.beeline.repository.ContactsRepo;
import com.buzz.taq.email.beeline.repository.JobsHistoryRepo;
import com.buzz.taq.email.beeline.utility.BeelineConstants;
import com.buzz.taq.email.beeline.utility.PrettyPrintingMap;

import lombok.extern.slf4j.Slf4j;

/**
 * The attachment file service.
 */
@Service
@Slf4j
public class AttachmentFileService {

	/** The attachment files repo. */
	@Autowired
	private AttachmentFilesRepo attachmentFilesRepo;

	/** The contacts repo. */
	@Autowired
	private ContactsRepo contactsRepo;

	/** The jobs history repo. */
	@Autowired
	private JobsHistoryRepo jobsHistoryRepo;

	/** The java mail sender. */
	@Autowired
	private JavaMailSender javaMailSender;

	/** The integration config repo. */
	@Autowired
	private IntegrationConfigRepo integrationConfigRepo;

	/**
	 * Post attachment file.
	 *
	 * @param attachmentFile
	 *            The attachment file.
	 * @return The simple entry.
	 */
	public SimpleEntry<String, String> postAttachmentFile(final MultipartFile attachmentFile) {
		String valid = "true";
		String message = "Successfully ";
		int createdJobs = 0;
		int updatedJobs = 0;
		int deactivatedJobs = 0;

		try (CSVParser parser = CSVParser.parse(new InputStreamReader(attachmentFile.getInputStream()),
				CSVFormat.EXCEL.withHeader(AttachmentHeader.class).withIgnoreEmptyLines().withTrim()
						.withFirstRecordAsHeader().withAllowMissingColumnNames(false).withDelimiter('|')
						.withQuote(null))) {
			final List<String> edittedJobsList = new ArrayList<>();
			final List<String> deactivatedJobsList = new ArrayList<>();
			final List<String> errorEncountered = new ArrayList<>();
			for (CSVRecord csvRecord : parser) {
				try {
					final String vmsId = csvRecord.get(AttachmentHeader.ID.value);
					final List<AttachmentFiles> attachmentFileResultList = attachmentFilesRepo.findByVmsId(vmsId);
					if (attachmentFileResultList.isEmpty()) {
						final AttachmentFiles attachmentFiles = new AttachmentFiles();
						attachmentFiles.setVendorId(BeelineConstants.VENDOR_ID);
						final Contacts contact = contactsRepo.findByNameAndEntityId(
								csvRecord.get(AttachmentHeader.MSP_Owner.value), BeelineConstants.VENDOR_ID);
						if (contact != null) {
							attachmentFiles.setVendorContactID(contact.getId());
						} else {
							final Contacts contactNew = new Contacts();
							contactNew.setEntityType("0");
							contactNew.setEmail("0");
							contactNew.setEntityId(BeelineConstants.VENDOR_ID);
							contactNew.setName(csvRecord.get(AttachmentHeader.MSP_Owner.value));
							contactNew.setPhone("0");
							contactNew.setProfile("0");
							contactNew.setRole("0");
							contactNew.setCreated(new Timestamp(System.currentTimeMillis()));
							contactNew.setCreatedBy(BeelineConstants.SYSTEM);
							contactNew.setUpdated(new Timestamp(System.currentTimeMillis()));
							contactNew.setUpdatedBy(BeelineConstants.SYSTEM);
							final Contacts contactResult = contactsRepo.save(contactNew);
							sendEmail("New Contact Added",
									"New contact is added and details are required " + contactResult.toString(),
									"akulindwa@buzzclan.net");
							attachmentFiles.setVendorContactID(contactResult.getId());
						}
						attachmentFiles.setTitle(csvRecord.get(AttachmentHeader.Job_Title.value));
						attachmentFiles.setCountry("US");
						attachmentFiles.setJobDesc("job description");
						String jd = csvRecord.get(AttachmentHeader.Job_Description.value);
						List<String> list = splitString(jd);
						if (list.size() == 1) {
							attachmentFiles.setJobDesc(list.get(0));
						}
						if (list.size() == 2) {
							attachmentFiles.setJobDesc(list.get(0));
							attachmentFiles.setRequirements(list.get(1));

						}
						if (list.size() == 3) {
							attachmentFiles.setJobDesc(list.get(0));
							attachmentFiles.setRequirements(list.get(1));
						}
						// attachmentFiles.setJobDesc(csvRecord.get(AttachmentHeader.Job_Description.value));
						attachmentFiles.setCity(csvRecord.get(AttachmentHeader.Work_Location.value));
						attachmentFiles.setClient(BeelineConstants.GENPACT);
						attachmentFiles
								.setRate(Double.valueOf(csvRecord.get(AttachmentHeader.Rate_Card.value).substring(1)));
						attachmentFiles.setExpense(expenseSet(csvRecord.get(AttachmentHeader.Estimated_Expense.value)));
						attachmentFiles
								.setStatus(String.valueOf(activeCheck(csvRecord.get(AttachmentHeader.Status.value))));
						attachmentFiles
								.setPositions(Long.valueOf(csvRecord.get(AttachmentHeader.Count_of_Resource.value)));
						attachmentFiles.setDuration(csvRecord.get(AttachmentHeader.Duration.value));
						attachmentFiles.setCreated(new Timestamp(System.currentTimeMillis()));
						attachmentFiles.setCreatedBy(BeelineConstants.SYSTEM);
						attachmentFiles.setActive(
								Character.valueOf(activeCheck(csvRecord.get(AttachmentHeader.Status.value))));
						Map<String, String> pon = new HashMap<>();
						if (csvRecord.get(AttachmentHeader.Job_Class.value) != null)
							pon.put(AttachmentHeader.Job_Class.toString(),
									csvRecord.get(AttachmentHeader.Job_Class.value));
						if (csvRecord.get(AttachmentHeader.Desired_Start_Date.value) != null)
							pon.put(AttachmentHeader.Desired_Start_Date.toString(),
									csvRecord.get(AttachmentHeader.Desired_Start_Date.value));
						if (csvRecord.get(AttachmentHeader.Submitted.value) != null)
							pon.put(AttachmentHeader.Submitted.toString(),
									csvRecord.get(AttachmentHeader.Submitted.value));
						if (csvRecord.get(AttachmentHeader.Received.value) != null)
							pon.put(AttachmentHeader.Received.toString(),
									csvRecord.get(AttachmentHeader.Received.value));
						if (csvRecord.get(AttachmentHeader.Work_Location.value) != null)
							pon.put(AttachmentHeader.Work_Location.toString(),
									csvRecord.get(AttachmentHeader.Work_Location.value));
						if (csvRecord.get(AttachmentHeader.Rate_Card_Type.value) != null)
							pon.put(AttachmentHeader.Rate_Card_Type.toString(),
									csvRecord.get(AttachmentHeader.Rate_Card_Type.value));
						if (csvRecord.get(AttachmentHeader.Default_Max_OT.value) != null)
							pon.put(AttachmentHeader.Default_Max_OT.toString(),
									csvRecord.get(AttachmentHeader.Default_Max_OT.value));
						if (csvRecord.get(AttachmentHeader.End_Date.value) != null)
							pon.put(AttachmentHeader.End_Date.toString(),
									csvRecord.get(AttachmentHeader.End_Date.value));
						if (csvRecord.get(AttachmentHeader.Estimated_Cost.value) != null)
							pon.put(AttachmentHeader.Estimated_Cost.toString(),
									csvRecord.get(AttachmentHeader.Estimated_Cost.value));
						if (csvRecord.get(AttachmentHeader.Estimated_Overtime_Hrs_Week.value) != null)
							pon.put(AttachmentHeader.Estimated_Overtime_Hrs_Week.toString(),
									csvRecord.get(AttachmentHeader.Estimated_Overtime_Hrs_Week.value));
						if (csvRecord.get(AttachmentHeader.Filled_Count.value) != null)
							pon.put(AttachmentHeader.Filled_Count.toString(),
									csvRecord.get(AttachmentHeader.Filled_Count.value));
						if (csvRecord.get(AttachmentHeader.Last_Name_First_Name_Middle_Name.value) != null)
							pon.put(AttachmentHeader.Last_Name_First_Name_Middle_Name.toString(),
									csvRecord.get(AttachmentHeader.Last_Name_First_Name_Middle_Name.value));
						if (csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_2_Full_Name.value) != null)
							pon.put(AttachmentHeader.Manager_Hierarchy_Level_2_Full_Name.toString(),
									csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_2_Full_Name.value));
						if (csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_3_Full_Name.value) != null)
							pon.put(AttachmentHeader.Manager_Hierarchy_Level_3_Full_Name.toString(),
									csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_3_Full_Name.value));
						if (csvRecord.get(AttachmentHeader.Secondary_Manager_Name_Last_Name_First_Name.value) != null)
							pon.put(AttachmentHeader.Secondary_Manager_Name_Last_Name_First_Name.toString(),
									csvRecord.get(AttachmentHeader.Secondary_Manager_Name_Last_Name_First_Name.value));
						attachmentFiles.setInternalComments(new PrettyPrintingMap<String, String>(pon).toString());
						attachmentFiles.setVmsId(csvRecord.get(AttachmentHeader.ID.value));
						attachmentFiles.setJobType(5);
						System.err.println("attachmentFiles:" + attachmentFiles.toString());
						attachmentFilesRepo.save(attachmentFiles);
						createdJobs++;
						if (attachmentFiles.getActive().equals('Y')) {
							deactivatedJobs++;
							deactivatedJobsList.add(attachmentFiles.getVmsId());
						}
					} else {
						final AttachmentFiles attachmentFileResult = attachmentFileResultList.get(0);
						final String currentStatus = attachmentFileResult.getStatus();
						final String newStatus = String
								.valueOf(activeCheck(csvRecord.get(AttachmentHeader.Status.value)));
						if (!newStatus.equals(currentStatus)) {
							updateJob(attachmentFileResult, csvRecord);
							edittedJobsList.add(attachmentFileResult.getVmsId());
							updatedJobs++;
						} else {
							if (!(currentStatus.equals(String.valueOf(activeCheck(BeelineConstants.STATUS_CANCELLED)))
									|| newStatus.equals(String.valueOf(activeCheck(BeelineConstants.STATUS_CANCELLED))))
									&& !(currentStatus
											.equals(String.valueOf(activeCheck(BeelineConstants.STATUS_FILLED)))
											|| newStatus.equals(
													String.valueOf(activeCheck(BeelineConstants.STATUS_FILLED))))) {
								if (!csvRecord.get(AttachmentHeader.Duration.value)
										.equals(attachmentFileResult.getDuration())
										|| !Double.valueOf(csvRecord.get(AttachmentHeader.Rate_Card.value).substring(1))
												.equals(attachmentFileResult.getRate())
										|| !expenseSet(csvRecord.get(AttachmentHeader.Estimated_Expense.value))
												.equals(attachmentFileResult.getExpense().trim())) {
									updateJob(attachmentFileResult, csvRecord);
									edittedJobsList.add(attachmentFileResult.getVmsId());
									updatedJobs++;

								}
							}
						}
					}
				} catch (Exception e) {
					System.err.println(e);
					errorEncountered.add(e.getMessage());
				}
			}
			String emailString = String.format(
					"Date : %s\nTask : %s\nNumber of new jobs added : %s\nNumber of jobs edited : %s\nList of ediited jobs with beeline job ID : %s\n"
							+ "Number of jobs deactivated : %s\nList of deactivated jobs with beeline job ID : %s\nError encountered :  %s\n",
					new Timestamp(System.currentTimeMillis()).toString(), BeelineConstants.ADDED_BEELINE_JOBS,
					String.valueOf(createdJobs), String.valueOf(updatedJobs), edittedJobsList.toString(),
					String.valueOf(deactivatedJobs), deactivatedJobsList.toString(), errorEncountered.toString());
			// System.err.println("Email String " + emailString);
			sendEmail("Beeline email response", emailString, "akulindwa@buzzclan.net");
		} catch (Exception exception) {
			// LOG.error("Couldn't post the data.", exception);
			System.err.println(exception);
			valid = "false";
			message = exception.getMessage();
		}
		return new SimpleEntry<String, String>(valid, message);
	}

	/**
	 * Validate attachment file.
	 *
	 * @param attachmentFile
	 *            The attachment file.
	 * @param name
	 *            The name.
	 * @return The simple entry.
	 */
	public SimpleEntry<String, String> validateAttachmentFile(final MultipartFile attachmentFile, final String name) {
		String valid = "false";
		String message = "Valid file format";
		final String messageTemplate = "column in a file can't be blank as it is mandatory field";
		boolean checkHeader = true;
		try (CSVParser parser = CSVParser.parse(new InputStreamReader(attachmentFile.getInputStream()),
				CSVFormat.DEFAULT.withHeader(AttachmentHeader.class).withIgnoreEmptyLines().withTrim()
						.withFirstRecordAsHeader().withAllowMissingColumnNames(false).withDelimiter('|')
						.withQuote(null))) {
			for (CSVRecord csvRecord : parser) {
				if (checkHeader) {
					csvRecord.get(AttachmentHeader.ID.value);
					csvRecord.get(AttachmentHeader.Request_Job_Position.value);
					csvRecord.get(AttachmentHeader.Job_Class.value);
					csvRecord.get(AttachmentHeader.Job_Title.value);
					csvRecord.get(AttachmentHeader.Desired_Start_Date.value);
					csvRecord.get(AttachmentHeader.Status.value);
					csvRecord.get(AttachmentHeader.Submitted.value);
					csvRecord.get(AttachmentHeader.Count_of_Resource.value);
					csvRecord.get(AttachmentHeader.Received.value);
					csvRecord.get(AttachmentHeader.MSP_Owner.value);
					csvRecord.get(AttachmentHeader.Work_Location.value);
					csvRecord.get(AttachmentHeader.Duration.value);
					csvRecord.get(AttachmentHeader.Rate_Card.value);
					csvRecord.get(AttachmentHeader.Rate_Card_Type.value);
					csvRecord.get(AttachmentHeader.Date.value);
					csvRecord.get(AttachmentHeader.Default_Max_OT.value);
					csvRecord.get(AttachmentHeader.Default_Max_Pay_Rate.value);
					csvRecord.get(AttachmentHeader.Default_Max_RT.value);
					csvRecord.get(AttachmentHeader.Duration_Days.value);
					csvRecord.get(AttachmentHeader.Duration_Months.value);
					csvRecord.get(AttachmentHeader.Duration_Weeks.value);
					csvRecord.get(AttachmentHeader.End_Date.value);
					csvRecord.get(AttachmentHeader.Estimated_Cost.value);
					csvRecord.get(AttachmentHeader.Estimated_Expense.value);
					csvRecord.get(AttachmentHeader.Estimated_Overtime_Hrs_Week.value);
					csvRecord.get(AttachmentHeader.Filled_Count.value);
					csvRecord.get(AttachmentHeader.Genpact_Job_Title.value);
					csvRecord.get(AttachmentHeader.Has_Submitted_Candidates.value);
					csvRecord.get(AttachmentHeader.Beeline_ID.value);
					csvRecord.get(AttachmentHeader.Last_Name_First_Name_Middle_Name.value);
					csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_2_Full_Name.value);
					csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_3_Full_Name.value);
					csvRecord.get(AttachmentHeader.Secondary_Manager_Name_Last_Name_First_Name.value);
					csvRecord.get(AttachmentHeader.Job_Description.value);
					checkHeader = false;
				}
				if (csvRecord.get(AttachmentHeader.Job_Title.value).isEmpty()) {
					return new SimpleEntry<String, String>(valid,
							String.format("%s %s", AttachmentHeader.Job_Title.value, messageTemplate));
				}
				if (csvRecord.get(AttachmentHeader.Job_Description.value).isEmpty()) {
					return new SimpleEntry<String, String>(valid,
							String.format("%s %s", AttachmentHeader.Job_Description.value, messageTemplate));
				}
				if (csvRecord.get(AttachmentHeader.Rate_Card.value).isEmpty()) {
					return new SimpleEntry<String, String>(valid,
							String.format("%s %s", AttachmentHeader.Rate_Card.value, messageTemplate));
				}
			}
		} catch (Exception exception) {
			log.error("Couldn't parse the attachment file.", exception);
			return new SimpleEntry<String, String>(valid, exception.getMessage());
		}
		valid = "true";
		return new SimpleEntry<String, String>(valid, message);
	}

	/**
	 * The Enum AttachmentHeader.
	 */
	private enum AttachmentHeader {

		ID("ID"),

		Request_Job_Position("Request - Job Position"),

		Job_Class("Job Class"),

		Job_Title("Job Title"),

		Desired_Start_Date("Desired Start Date"),

		Status("Status"),

		Submitted("Submitted"),

		Count_of_Resource("Count of Resource"),

		Received("Received"),

		MSP_Owner("MSP Owner"),

		Work_Location("Work Location"),

		Duration("Duration"),

		Rate_Card("Rate Card"),

		Rate_Card_Type("Rate Card Type"),

		Date("Date"),

		Default_Max_OT("Default Max OT"),

		Default_Max_Pay_Rate("Default Max Pay Rate"),

		Default_Max_RT("Default Max RT"),

		Duration_Days("Duration Days"),

		Duration_Months("Duration Months"),

		Duration_Weeks("Duration Weeks"),

		End_Date("End Date"),

		Estimated_Cost("Estimated Cost"),

		Estimated_Expense("Estimated Expense"),

		Estimated_Overtime_Hrs_Week("Estimated Overtime Hrs/Week"),

		Filled_Count("Filled Count"),

		Genpact_Job_Title("Genpact Job Title"),

		Has_Submitted_Candidates("Has Submitted Candidates"),

		Beeline_ID("Beeline ID"),

		Last_Name_First_Name_Middle_Name("Last Name, First Name Middle Name"),

		Manager_Hierarchy_Level_2_Full_Name("Manager Hierarchy Level 2 - Full Name"),

		Manager_Hierarchy_Level_3_Full_Name("Manager Hierarchy Level 3 - Full Name"),

		Secondary_Manager_Name_Last_Name_First_Name("Secondary Manager Name (Last Name, First Name)"),

		Job_Description("Job Description");

		String value;

		AttachmentHeader(String value) {
			this.value = value;
		}

	}

	private char activeCheck(final String status) {

		if (status != null) {
			if (status.trim().equalsIgnoreCase(BeelineConstants.STATUS_OPEN)) {
				return 'Y';
			}
			if (status.trim().equalsIgnoreCase(BeelineConstants.STATUS_CANCELLED)
					|| status.trim().equalsIgnoreCase(BeelineConstants.STATUS_FILLED)) {
				return 'N';
			}
			if (status.trim().equalsIgnoreCase(BeelineConstants.STATUS_ONHOLD)) {
				return 'C';
			}
		}
		return 0;
	}

	private List<String> splitString(String jobDescription) {
		List<String> jobDes = new ArrayList<>();
		if (jobDescription != null) {
			int length = jobDescription.length();
			if (length > 2000 && length <= 4000) {
				jobDes.add(jobDescription.substring(0, 1999));
				jobDes.add(jobDescription.substring(2000, length - 1));
				return jobDes;
			}

			if (length > 4000 && length <= 6000) {
				jobDes.add(jobDescription.substring(0, 1999));
				jobDes.add(jobDescription.substring(2000, 3999));
				jobDes.add(jobDescription.substring(4000, length - 1));
				return jobDes;
			}
			if (length > 6000) {
				jobDes.add(jobDescription.substring(0, 1999));
				jobDes.add(jobDescription.substring(2000, 3999));
				jobDes.add(jobDescription.substring(4000, 5999));
				return jobDes;
			}
		}

		jobDes.add(jobDescription);
		return jobDes;
	}

	private void saveToJobHistory(final AttachmentFiles attachment) {

		final JobsHistory jobsHistory = new JobsHistory();
		jobsHistory.setActive(attachment.getActive());
		jobsHistory.setAutoExpire(attachment.getAutoExpire());
		jobsHistory.setCity(attachment.getCity());
		jobsHistory.setClient(attachment.getClient());
		jobsHistory.setCountry(attachment.getCountry());
		jobsHistory.setCreated(attachment.getCreated());
		jobsHistory.setCreatedBy(attachment.getCreatedBy());
		jobsHistory.setDuration(attachment.getDuration());
		jobsHistory.setExpense(attachment.getExpense());
		jobsHistory.setId(attachment.getId());
		jobsHistory.setInternalComments(attachment.getInternalComments());
		jobsHistory.setJbSource(attachment.getJbSource());
		jobsHistory.setJobDesc(attachment.getJobDesc());
		jobsHistory.setJobType(attachment.getJobType());
		jobsHistory.setPositions(attachment.getPositions());
		jobsHistory.setPublish(attachment.getPublish());
		jobsHistory.setRate(attachment.getRate());
		jobsHistory.setRequirements(attachment.getRequirements());
		jobsHistory.setStateCD(attachment.getStateCD());
		jobsHistory.setStatus(attachment.getStatus());
		jobsHistory.setSubmissionLimit(attachment.getSubmissionLimit());
		jobsHistory.setTag(attachment.getTag());
		jobsHistory.setTitle(attachment.getTitle());
		jobsHistory.setUpdated(attachment.getUpdated());
		jobsHistory.setUpdatedBy(attachment.getUpdatedBy());
		jobsHistory.setVendorContactID(attachment.getVendorContactID());
		jobsHistory.setVendorId(attachment.getVendorId());
		jobsHistory.setVisaRestriction(attachment.getVisaRestriction());
		jobsHistory.setVmsId(attachment.getVmsId());
		jobsHistory.setZip(attachment.getZip());
		jobsHistoryRepo.save(jobsHistory);
	}

	private void sendEmail(final String subject, final String text, final String from) {

		final SimpleMailMessage msg = new SimpleMailMessage();
		msg.setSubject(subject);
		msg.setText(text);
		final IntegrationConfig integrationConfig = integrationConfigRepo.findByKey("email");
		String[] to = null;
		if (integrationConfig != null) {
			to = integrationConfig.getValue().split(",");
		} else {
			to = new String[] { "sachin@buzzclan.com" };
		}
		msg.setTo(to);
		msg.setFrom(from);
		javaMailSender.send(msg);

	}

	private void updateJob(final AttachmentFiles attachmentFileResult, final CSVRecord csvRecord) {

		// saveToJobHistory(attachmentFileResult);
		attachmentFileResult.setVendorId(BeelineConstants.VENDOR_ID);
		final Contacts contact = contactsRepo.findByNameAndEntityId(csvRecord.get(AttachmentHeader.MSP_Owner.value),
				BeelineConstants.VENDOR_ID);
		if (contact != null) {
			attachmentFileResult.setVendorContactID(contact.getId());
		} else {
			final Contacts contactNew = new Contacts();
			contactNew.setEntityType("0");
			contactNew.setEmail("0");
			contactNew.setEntityId(BigInteger.valueOf(0));
			contactNew.setName(csvRecord.get(AttachmentHeader.MSP_Owner.value));
			contactNew.setPhone("0");
			contactNew.setProfile("0");
			contactNew.setRole("0");
			contactNew.setCreated(new Timestamp(System.currentTimeMillis()));
			contactNew.setCreatedBy(BeelineConstants.SYSTEM);
			contactNew.setUpdated(new Timestamp(System.currentTimeMillis()));
			contactNew.setUpdatedBy(BeelineConstants.SYSTEM);
			final Contacts contactResult = contactsRepo.save(contactNew);
			sendEmail("New Contact Added", "New contact is added and details are required " + contactResult.toString(),
					"akulindwa@buzzclan.net");
		}
		attachmentFileResult.setTitle(csvRecord.get(AttachmentHeader.Job_Title.value));
		attachmentFileResult.setCountry("US");
		String jd = csvRecord.get(AttachmentHeader.Job_Description.value);
		List<String> list = splitString(jd);
		if (list.size() == 1) {
			attachmentFileResult.setJobDesc(list.get(0));
		}
		if (list.size() == 2) {
			attachmentFileResult.setJobDesc(list.get(0));
			attachmentFileResult.setRequirements(list.get(1));

		}
		if (list.size() == 3) {
			attachmentFileResult.setJobDesc(list.get(0));
			attachmentFileResult.setRequirements(list.get(1));
		}
		// attachmentFileResult.setJobDesc(csvRecord.get(AttachmentHeader.Job_Description.value));
		attachmentFileResult.setCity(csvRecord.get(AttachmentHeader.Work_Location.value));
		attachmentFileResult.setClient(BeelineConstants.GENPACT);
		attachmentFileResult.setRate(Double.valueOf(csvRecord.get(AttachmentHeader.Rate_Card.value).substring(1)));
		attachmentFileResult.setExpense(expenseSet(csvRecord.get(AttachmentHeader.Estimated_Expense.value)));
		attachmentFileResult.setPositions(Long.valueOf(csvRecord.get(AttachmentHeader.Count_of_Resource.value)));
		attachmentFileResult.setDuration(csvRecord.get(AttachmentHeader.Duration.value));
		attachmentFileResult.setUpdated(new Timestamp(System.currentTimeMillis()));
		attachmentFileResult.setUpdatedBy(BeelineConstants.SYSTEM);
		attachmentFileResult.setActive(Character.valueOf(activeCheck(csvRecord.get(AttachmentHeader.Status.value))));
		Map<String, String> pon = new HashMap<>();
		if (csvRecord.get(AttachmentHeader.Job_Class.value) != null)
			pon.put(AttachmentHeader.Job_Class.toString(), csvRecord.get(AttachmentHeader.Job_Class.value));
		if (csvRecord.get(AttachmentHeader.Desired_Start_Date.value) != null)
			pon.put(AttachmentHeader.Desired_Start_Date.toString(),
					csvRecord.get(AttachmentHeader.Desired_Start_Date.value));
		if (csvRecord.get(AttachmentHeader.Submitted.value) != null)
			pon.put(AttachmentHeader.Submitted.toString(), csvRecord.get(AttachmentHeader.Submitted.value));
		if (csvRecord.get(AttachmentHeader.Received.value) != null)
			pon.put(AttachmentHeader.Received.toString(), csvRecord.get(AttachmentHeader.Received.value));
		if (csvRecord.get(AttachmentHeader.Work_Location.value) != null)
			pon.put(AttachmentHeader.Work_Location.toString(), csvRecord.get(AttachmentHeader.Work_Location.value));
		if (csvRecord.get(AttachmentHeader.Rate_Card_Type.value) != null)
			pon.put(AttachmentHeader.Rate_Card_Type.toString(), csvRecord.get(AttachmentHeader.Rate_Card_Type.value));
		if (csvRecord.get(AttachmentHeader.Default_Max_OT.value) != null)
			pon.put(AttachmentHeader.Default_Max_OT.toString(), csvRecord.get(AttachmentHeader.Default_Max_OT.value));
		if (csvRecord.get(AttachmentHeader.End_Date.value) != null)
			pon.put(AttachmentHeader.End_Date.toString(), csvRecord.get(AttachmentHeader.End_Date.value));
		if (csvRecord.get(AttachmentHeader.Estimated_Cost.value) != null)
			pon.put(AttachmentHeader.Estimated_Cost.toString(), csvRecord.get(AttachmentHeader.Estimated_Cost.value));
		if (csvRecord.get(AttachmentHeader.Estimated_Overtime_Hrs_Week.value) != null)
			pon.put(AttachmentHeader.Estimated_Overtime_Hrs_Week.toString(),
					csvRecord.get(AttachmentHeader.Estimated_Overtime_Hrs_Week.value));
		if (csvRecord.get(AttachmentHeader.Filled_Count.value) != null)
			pon.put(AttachmentHeader.Filled_Count.toString(), csvRecord.get(AttachmentHeader.Filled_Count.value));
		if (csvRecord.get(AttachmentHeader.Last_Name_First_Name_Middle_Name.value) != null)
			pon.put(AttachmentHeader.Last_Name_First_Name_Middle_Name.toString(),
					csvRecord.get(AttachmentHeader.Last_Name_First_Name_Middle_Name.value));
		if (csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_2_Full_Name.value) != null)
			pon.put(AttachmentHeader.Manager_Hierarchy_Level_2_Full_Name.toString(),
					csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_2_Full_Name.value));
		if (csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_3_Full_Name.value) != null)
			pon.put(AttachmentHeader.Manager_Hierarchy_Level_3_Full_Name.toString(),
					csvRecord.get(AttachmentHeader.Manager_Hierarchy_Level_3_Full_Name.value));
		if (csvRecord.get(AttachmentHeader.Secondary_Manager_Name_Last_Name_First_Name.value) != null)
			pon.put(AttachmentHeader.Secondary_Manager_Name_Last_Name_First_Name.toString(),
					csvRecord.get(AttachmentHeader.Secondary_Manager_Name_Last_Name_First_Name.value));
		attachmentFileResult.setInternalComments(new PrettyPrintingMap<String, String>(pon).toString());
		attachmentFileResult.setJobType(5);
		System.err.println("attachmentFileResult: " + attachmentFileResult.toString());
		attachmentFilesRepo.save(attachmentFileResult);

	}

	private String expenseSet(final String expense) {
		if (expense != null) {
			final String expensePart = expense.substring(1, 2);
			if (expensePart.equals("0")) {
				return "N";
			}
			return expensePart;
		}
		return null;
	}

}
