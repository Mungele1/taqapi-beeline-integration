package com.buzz.taq.email.beeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "com.buzz.taq.config.integrationconfig", "com.buzz.taq.email.beeline" })
public class BeelineApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {

		SpringApplication.run(BeelineApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(BeelineApplication.class);
	}

}
