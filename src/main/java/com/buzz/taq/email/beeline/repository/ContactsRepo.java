package com.buzz.taq.email.beeline.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.buzz.taq.email.beeline.entity.Contacts;

/**
 * The Interface ContactsRepo.
 *
 */
@Repository
public interface ContactsRepo extends CrudRepository<Contacts, BigInteger> {

	Contacts findByNameAndEntityId(String name, BigInteger entityId);

}
