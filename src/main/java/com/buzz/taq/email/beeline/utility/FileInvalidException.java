package com.buzz.taq.email.beeline.utility;

/**
 * @author RamanG
 *
 */
public class FileInvalidException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 *            the message.
	 */
	public FileInvalidException(final String message) {
		super(message);
	}
}
