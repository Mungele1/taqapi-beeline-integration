package com.buzz.taq.email.beeline.api;

import java.util.AbstractMap.SimpleEntry;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MultipartFile;

import com.buzz.taq.email.beeline.models.StandardResponse;
import com.buzz.taq.email.beeline.service.AttachmentFileService;
import com.buzz.taq.email.beeline.utility.FileInvalidException;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class AttachmentFilesControllerApiController.
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-09-10T03:46:31.592-05:00")

@RestController
@Slf4j
public class AttachmentFilesControllerApiController implements AttachmentFilesControllerApi {

	/** The attachment file service. */
	@Autowired
	private AttachmentFileService attachmentFileService;

	/**
	 * Post attachment file.
	 *
	 * @param name
	 *            The name.
	 * @param uploadfile
	 *            The upload file.
	 * @param description
	 *            The description.
	 * @return The response entity.
	 */
	@Override
	public ResponseEntity<StandardResponse> postAttachmentFile(
			@ApiParam(value = "file detail") @Valid @RequestPart("uploadfile") final MultipartFile uploadfile) {
		String message = null;
		String name = "file";
		try {
			SimpleEntry<String, String> valid = attachmentFileService.validateAttachmentFile(uploadfile, name.trim());
			message = valid.getValue();
			if ("false".equals(valid.getKey().trim().toLowerCase())) {
				throw new FileInvalidException(message);
			}
			valid = attachmentFileService.postAttachmentFile(uploadfile);
			message = valid.getValue();
			if ("false".equals(valid.getKey().trim().toLowerCase())) {
				throw new RestClientException(message);
			}
			return new ResponseEntity<>(new StandardResponse().success(Boolean.valueOf(true))
					.message("Uploaded file successfully processed."), HttpStatus.CREATED);
		} catch (final FileInvalidException e) {
			//log.error(message);
			return new ResponseEntity<>(new StandardResponse().success(Boolean.valueOf(false)).message(message),
					HttpStatus.OK);
		}
	}

}
